//shortcuts
//alt + shift + f - clean code
//ctrl + shift + space - show function signatures
//ctrl + shift + P - open command window
//ctrl + . - on class definition while implementing interface

import { Category } from './enums';
import { Book, Librarian, Logger, Author, Magazine } from './interfaces';
import { schoolLibrarian } from './classes';
import Encyclopedia from './enciclopedia';
import { purge } from './lib/utility-functions';
import Shelf from './shelf';

const inventory: Book[] = [
    { id: 10, title: 'The C Programming Language', author: 'K & R', available: true, category: Category.Software },
    { id: 11, title: 'Code Complete', author: 'Steve McConnell', available: true, category: Category.Software },
    { id: 12, title: '8-Bit Graphics with Cobol', author: 'A. B.', available: true, category: Category.Software },
    { id: 13, title: 'Cool autoexec.bat Scripts!', author: 'C. D.', available: true, category: Category.Software }
];

function getAllBooks(): Book[] {
    return [
        { id: 1, title: 'Refactoring JavaScript', author: 'Evan Burchard', available: true, category: Category.JavaScript },
        { id: 2, title: 'JavaScript Testing', author: 'Liang Yuxian Eugene', available: false, category: Category.JavaScript },
        { id: 3, title: 'CSS Secrets', author: 'Lea Verou', available: true, category: Category.CSS },
        { id: 4, title: 'Mastering JavaScript Object-Oriented Programming', author: 'Andrea Chiarelli', available: true, category: Category.JavaScript }
    ];
}

function logFirstAvailable(books: Book[] = getAllBooks()): void {
    const booksCount: number = books.length;
    let firstAvailableBook: string = '';

    for (let book of books) {
        if (book.available) {
            firstAvailableBook = book.title;
            break;
        }
    }
    console.log(`Total book count: ${booksCount}`);
    console.log(`First available ${firstAvailableBook}`);
}

function getBookTitlesCategory(category: Category = Category.JavaScript): string[] {
    const books: Book[] = getAllBooks();

    return books.filter(item => item.category === category).map(({ title }) => title);
}

function logBookTitles(bookTitles: string[]): void {
    console.log(bookTitles);
}

function getBookById(id: number): Book | undefined {
    const books: Book[] = getAllBooks();

    return books.find(book => book.id === id);
}

function createCustomerId(name: string, id: number): string {
    return `${name}${id}`;
}

let idGenerator: (name: string, id: number) => string;
idGenerator = createCustomerId;

function createCustomer(name: string, age?: number, city?: string): void {
    console.log(`Customer ${name}`);

    if (age) {
        console.log(`Age ${age}`);
    }
    if (city) {
        console.log(`City ${city}`);
    }
}

function getTitles(author: string): string[];
function getTitles(available: boolean): string[];
function getTitles(bookProperty: any): string[] {
    const allBooks: Book[] = getAllBooks();
    const foundedTitles: string[] = [];

    if (typeof bookProperty === 'string') {
        for (let book of allBooks) {
            if (book.author === bookProperty) {
                foundedTitles.push(book.title);
            }
        }
    } else if (typeof bookProperty === 'boolean') {
        for (let book of allBooks) {
            if (book.available === bookProperty) {
                foundedTitles.push(book.title);
            }
        }
    }

    return foundedTitles;
}

function printBook(book: Book) {
    console.log(`${book.title} by ${book.author}`);
}

//------------------------------------------------------------
const allBooks: Book[] = getAllBooks();
logFirstAvailable(allBooks);

//------------------------------------------------------------
const allJSBooks: string[] = getBookTitlesCategory(Category.JavaScript);
logBookTitles(allJSBooks);

//------------------------------------------------------------
console.log(getBookById(4));

//------------------------------------------------------------
let myId: string = createCustomerId('Ann', 10);
console.log(myId);

//------------------------------------------------------------
createCustomer('Nick');
createCustomer('Nick', 26);
createCustomer('Nick', 26, 'Kiev');

//------------------------------------------------------------
let checkedOutBooks: string[] = getTitles(false);
let checkedOutBooks1: string[] = getTitles('Evan Burchard');
console.log(checkedOutBooks, checkedOutBooks1);

//------------------------------------------------------------
let myBook: Book = {
    id: 1,
    title: 'Refactoring JavaScript',
    author: 'Evan Burchard',
    available: true,
    category: Category.JavaScript,
    year: 2015,
    copies: 3,
    markDamaged: function (reason: string): string {
        return reason;
    }
};

printBook(myBook);

//------------------------------------------------------------
let damageFn: Logger = myBook.markDamaged || function (reason: string): string { return reason; };
damageFn('mark');

//------------------------------------------------------------
let favouriteAuthor: Author = {
    email: 'asdfa@epam.com',
    name: 'asdfa',
    numberBookPublished: 3
};

let favouriteLibrarian: Librarian = {
    email: 'asdfa@epam.com',
    name: 'asdfa',
    department: 'some departament',
    assistCustomer(customerName: string) {
        console.log(`Assist ${customerName}`);
    }
};

//--------------------------------------------------------

class Novel extends class { title: string = 'asdf1' } {
    mainCharacter: string = 'asdf';
}

// let ref: ReferenceItem = new ReferenceItem('title', 2017);
// ref.printItem();
// ref.publisher = 'Anna';
// console.log(ref.publisher);
let refBook: Encyclopedia = new Encyclopedia('our title', 2015, 10);
refBook.printItem();
console.log(refBook.edition);

let novel = new Novel();

console.log(novel);

//--------------------------------------------------------

console.log(purge<Book>(inventory));
console.log(purge<number>([1, 2, 3, 4, 5]));

//--------------------------------------------------------
let bookShelf: Shelf<Book> = new Shelf<Book>();
bookShelf.add(inventory);
console.log(`bookShelf`, bookShelf.getFirst());

let magazines = [
    { title: 'Programming Language Monthly', publisher: 'Code Mags' },
    { title: 'Literary Fiction Quarterly', publisher: 'College Press' },
    { title: 'Five Points', publisher: 'GSU' }
];

let magazinesShelf: Shelf<Magazine> = new Shelf<Magazine>();
magazinesShelf.add(magazines);
console.log(`magazinesShelf`, magazinesShelf.getFirst());
magazinesShelf.printTitles();

//--------------------------------------------------------

let librarian = new schoolLibrarian('some', 'asdfasfd', 'asdfasdf');
librarian['aaaa'] = 2;
console.log(librarian);

//--------------------------------------------------------
interface IBird {
    fly(): void;
    layEggs(): void;
}

interface IFish {
    swim(): void;
    layEggs(): void;
}

class Bird implements IBird {
    public fly() {}
    public layEggs() {}
}

class Fish implements IFish {
    public swim() {}
    public layEggs() {}
}

function isFish(pet: Fish | Bird): pet is Fish {
    return (<Fish>pet).swim !== undefined;
}

function getSmallPet(param: Fish | Bird): Fish | Bird {
    return param;
}

let pet = getSmallPet(new Fish());

// Each of these property accesses will cause an error
if (isFish(pet)) {
    pet.swim();
}
else pet.fly();

function isNumber(x: any): x is number {
    return typeof x === "number";
}

function isString(x: any): x is string {
    return typeof x === "string";
}

function padLeft(value: string, padding: string | number) {
    if (typeof padding === 'number') {
        return Array(padding + 1).join(" ") + value;
    }
    if (isString(padding)) {
        return padding + value;
    }
    throw new Error(`Expected string or number, got '${padding}'.`);
}

type Alias = { num: number }
interface Interface {
    num: number;
}
declare function aliased(arg: Alias): Alias;
declare function interfaced(arg: Interface): Interface;

interface Square {
    kind: "square";
    size: number;
}
interface Rectangle {
    kind: "rectangle";
    width: number;
    height: number;
}
interface Circle {
    kind: "circle";
    radius: number;
}

interface Triangle {
    kind: "triangle";
    radius: number;
}

type Shape = Square | Rectangle | Circle | Triangle;

function assertNever(x: never): never {
    throw new Error("Unexpected object: " + x);
}

function area(s: Shape) {
    switch (s.kind) {
        case "square": return s.size * s.size;
        case "rectangle": return s.height * s.width;
        case "circle": return Math.PI * s.radius ** 2;
        case "triangle": return Math.PI * s.radius ** 2;
        default: return assertNever(s); // error here if there are missing cases
    }
}

interface Person {
    name: string;
    age: number;
}


type Partial<T> = {
    [P in keyof T]?: T[P];
}

type PersonPartial = Partial<Person>;
type ReadonlyPerson = Readonly<Person>;

let personalPartial: ReadonlyPerson = {
   name: 'asdfa',
   age: 2
};

var props = ['name', 'age'];

type Proxy<T> = {
    get(): T;
    set(value: T): void;
}
type Proxify<T> = {
    [P in keyof T]: Proxy<T[P]>;
}

function proxify<T>(o: T): Proxify<T> {
    const handler = {
        get(target: any, key: string) {
            console.log(target[key]);
            return target[key];
        },
        set(target: any) {
            return true;
        }
    };
    return new Proxy(o, handler);
}

let persona: Person = {
   name: 'asdfa',
   age: 2
};

let proxyProps = proxify<Person>(persona);

