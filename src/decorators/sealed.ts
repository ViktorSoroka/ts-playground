export function sealed (param: string) {
    return function (target: Function) {
        console.log(`Sealing the constructor ${param}`);
        Object.seal(target);
        Object.seal(target.prototype);
    };
}

export function logger<TFunction extends Function>(target: TFunction): TFunction {
    let newConstr: Function = function () {
        console.log(`Creating new instance.`);
        console.log(`Class name: ${target.name}`);
    };

    newConstr.prototype = Object.create(target.prototype);
    newConstr.prototype.constructor = target;

    return <TFunction>newConstr;
}