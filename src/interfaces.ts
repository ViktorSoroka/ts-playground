import { Category } from './enums';

// interface for functional type
interface damageLogger {
    (reason: string): string
}

// model interface
interface Book {
    id: number;
    title: string;
    author: string;
    available: boolean;
    category: Category;
    copies?: number;
    year?: number;
    // markDamaged?: (reason: string) => string;
    markDamaged?: damageLogger
}

interface Person {
    name: string;
    email: string;
}

interface Author extends Person {
    numberBookPublished: number;
}

interface Librarian extends Person {
    department: string;
    assistCustomer: (custName: string) => void;
}

interface Magazine {
    title: string;
    publisher: string;
}

export {damageLogger as Logger, Author, Librarian, Person, Book, Magazine};