import * as Interfaces from './interfaces';
import { sealed } from './decorators/sealed';

@sealed('my first ts decorator')
class schoolLibrarian implements Interfaces.Librarian {
    public department;
    public name;
    public email;

    constructor(department, name, email) {
        this.department = department;
        this.name = name;
        this.email = email;
    }

    assistCustomer(custName: string): void {
        console.log(`Assist ${custName}`);
    }
}

abstract class ReferenceItem {
    // title: string;
    // year: number;

    // constructor(newTitle: string, newYear: number) {
    //     this.title = newTitle;
    //     this.year = newYear;
    // }

    private _publisher: string;

    constructor(
        public title: string,
        protected year: number
    ) { }

    static departament: string = 'Research';

    printItem(): void {
        console.log(`${this.title} was published in ${this.year} in ${ReferenceItem.departament}`);
    }

    abstract printCitation (): void;

    get publisher(): string {
        return this._publisher.toUpperCase();
    }

    set publisher(newPublisher) {
        this._publisher = newPublisher;
    }
}

export { ReferenceItem, schoolLibrarian };